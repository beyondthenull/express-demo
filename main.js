// Load express
var express = require('express');

// Create app
var app = express();

// Port
var port = 8080 || process.env.PORT;

// Serve files in www directory (index.html is root)
app.use(express.static('www'));

// Respond with message
app.get('/hello-world', function (req, res) {
  return res.end('Hello, World!');
});

// Read name and respond accordingly
app.get('/hello', function (req, res) {
  var name = req.query.name;

  if(!name) {
    return res.end('You didn\'t tell me your name!');
  }
  else {
    return res.end('Hello ' + name + '!')
  }
})

// Start server
app.listen(port, function () {
  console.log('Server started. Listening on port ' + port);
});
