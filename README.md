#Express Demo

##Description
Quick example of how to use express to make a web server

##How to run
1. Install latest version of node.js with the default options (https://nodejs.org)
Note: It is strongly suggested to add NodeJS to your PATH when asked
1. Download a copy of this repository
1. Open a command line and change to the repository directory (extract if needed)
1. Run `npm install`
1. Run `node main.js`
1. Open a webbrowser and navigate to http://localhost:8080
